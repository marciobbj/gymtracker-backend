from django.shortcuts import render
from rest_framework import viewsets, mixins
from .models import Weekdays
from .serializers import WeekdaysSerializer

# Create your views here.

class WeekDaysViewSet(mixins.ListModelMixin,
                      mixins.CreateModelMixin,
                      mixins.UpdateModelMixin,
                      mixins.RetrieveModelMixin,
                      viewsets.GenericViewSet):

    serializer_class = WeekdaysSerializer

    lookup_url_kwarg = 'id'

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['user_id'] = self.kwargs['user_uuid']
        return context

    def get_queryset(self):
        return Weekdays.objects.filter(user_id=self.kwargs['user_uuid'])

    def create(self, request, *args, **kwargs):
        self.serializer_class = WeekdaysSerializer
        return super().create(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)
