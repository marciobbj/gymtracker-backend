from django.db import models
from django.contrib.auth import get_user_model
from ted.trainings.models import Exercise

# Create your models here.

User = get_user_model()

class Weekdays(models.Model):
    monday = 'monday'
    tuesday = 'tuesday'
    wednesday = 'wednesday'
    thursday = 'thursday'
    friday = 'friday'
    saturday = 'saturday'
    sunday = 'sunday'

    WEEKDAYS = (
        (monday, 'Monday'),
        (tuesday, 'Tuesday'),
        (wednesday, 'Wednesday'),
        (thursday, 'Thursday'),
        (friday, 'Friday'),
        (saturday, 'Saturday'),
        (sunday, 'Sunday'),
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    weekday = models.CharField(max_length=255, choices=WEEKDAYS)
    color = models.CharField(max_length=255)
    exercises = models.ManyToManyField(Exercise)
