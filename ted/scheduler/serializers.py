from rest_framework import serializers
from rest_framework.response import Response

from .models import Weekdays


class WeekdaysSerializer(serializers.ModelSerializer):

    class Meta:
        model = Weekdays
        fields = ('__all__')
