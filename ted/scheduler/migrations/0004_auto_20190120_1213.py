# Generated by Django 2.1.5 on 2019-01-20 12:13

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('scheduler', '0003_auto_20190120_1157'),
    ]

    operations = [
        migrations.AlterField(
            model_name='weekdays',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
