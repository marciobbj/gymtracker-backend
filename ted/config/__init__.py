from .local import Local  # noqa
from .production import Production  # noqa
import pymysql

pymysql.install_as_MySQLdb()
