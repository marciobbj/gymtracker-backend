from django.db import models

# Create your models here.


class Exercise(models.Model):

    name = models.CharField(max_length=255)
    exercise_type = models.CharField(max_length=255)
    repetitions = models.IntegerField()
    recommendations = models.TextField()

    def __str__(self):
        return self.name
