from django.apps import AppConfig


class TrainingsConfig(AppConfig):
    name = 'ted.trainings'
